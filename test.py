from sudoku_solver import solver
from util import process, process_solution
from lp import solve
import numpy as np
import pandas
import time

if __name__ == "__main__":
    df1 = pandas.read_csv("data/large1.csv")
    df2 = pandas.read_csv("data/large2.csv")
    df = pandas.concat((df1, df2))
    data = df.sample(n=2000, random_state=42)
    corr_cnt = 0
    start = time.time()
    for round, (q, s) in enumerate(zip(data["quizzes"], data["solutions"])):
        quiz = q
        solu = s
        entries = process(quiz)
        t = solve(entries)
        if not np.all(t == process_solution(solu)):
            pass
        else:
            corr_cnt += 1

        if (round+1) % 5 == 0:
            end = time.time()
            print("Aver Time: {t:6.2f} secs. Success rate: {corr} / {all} ".format(
                t=(end-start)/(round+1), corr=corr_cnt, all=round+1))

    end = time.time()
    print("Aver Time: {t:6.2f} secs. Success rate: {corr} / {all} ".format(
        t=(end-start)/(round+1), corr=corr_cnt, all=round+1))
